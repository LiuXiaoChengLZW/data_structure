//
//  main.cpp
//  图的广度优先
//
//  Created by 刘晨 on 2018/11/26.
//  Copyright © 2018 刘晨. All rights reserved.
//

#include <iostream>
//#include "Cqueue.hpp"
#include "Graph.hpp"
using namespace std;
int main(){
//        Cqueue a;
//        a.inQueue(3);
//        a.inQueue(4);
//        a.inQueue(5);
//        a.printCqueue();
//        a.outQueue();
//        a.printCqueue();
//        a.outQueue();
//        a.printCqueue();
//        for (int i = 23; i<40; i++) {
//            a.inQueue(i);
//        }
//        a.printCqueue();
    Graph s;
    s.init(4, 4);
    s.create();
    s.printGraph();
//    s.addEdge('b', 'e');
//    s.printGraph();
//    s.deleteEdege('b', 'e');
//    s.printGraph();
//    s.breadthFirstSearch('d');
//    s.breadthFirstSearch('b');
    s.breadthFirstSearch('a');
    
    return 1;
}


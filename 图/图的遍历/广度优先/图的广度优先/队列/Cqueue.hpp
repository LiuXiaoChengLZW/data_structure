//
//  Graph.hpp
//  图的广度优先
//
//  Created by 橘子和香蕉 on 2018/11/26.
//  Copyright © 2018 橘子和香蕉. All rights reserved.
//


/*
 这这里用链表的形式来实现队列；
        队列的实现有两种，一种是数组，循环队列。一种是链表
        两个指针，一个指向头，一个指向尾。数据是在尾指针添加，在头指针删除。
 */
#ifndef Cqueue_hpp
#define Cqueue_hpp

#include <stdio.h>
#include <iostream>
typedef struct qnode{
    int position;
    qnode *next;
}qnode;

class Cqueue{
private:
    qnode *front;//头指针
    qnode *rear;//尾指针
    int length;//队列的长度
public:
    Cqueue();
    ~Cqueue();
    void inQueue(int data);//data入队
    int  outQueue();//出队列
    void printCqueue();//输出队列
    bool isEmpty();//判断队列是不是空
};
#endif /* Cqueue_hpp */

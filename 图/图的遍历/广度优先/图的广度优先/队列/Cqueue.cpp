//
//  Graph.cpp
//  图的广度优先
//
//  Created by 刘晨 on 2018/11/26.
//  Copyright © 2018 刘晨. All rights reserved.
//

#include "Cqueue.hpp"
using namespace std;
Cqueue::Cqueue(){
    front =  new qnode;
    front->next = NULL;
    rear = front;
    length = 0;
}
Cqueue::~Cqueue(){
    qnode *p;
    while (front != NULL ) {
        p = front;
        front = front->next;
        delete p;
    }
}
void Cqueue::inQueue(int data){
//    qnode *n = new qnode;
//    n->position = data;
//    n->next = rear->next;
//    rear->next = n;
//    rear = n;
//    if(front ->next == NULL ){
//        front->next = n;
//    }
//    length ++;
    qnode *p = new qnode;
    p->position = data;
    p->next = NULL;
    rear->next = p;
    rear = p;
    length++;
    

}
int Cqueue::outQueue(){
    if(front == rear){
        cout<<"error\n";
        return -1;
    }
//    qnode *p = front ->next;
//    int data = p->position;
//    front->next = p->next;
//    if(p->next == NULL){
//        rear = front;
//    }
//    delete p;
//    length--;
//    return data;
    qnode *p = front->next;
    int data = p->position;
    front->next = p->next;
    if(p->next ==NULL){
        rear = front;
    }
    delete p;
    length--;
    return data;
}
void Cqueue::printCqueue(){
    qnode *p = front->next;
    while (p != NULL) {
        cout<<p->position<<"\t";
        p = p->next;
    }
    cout<<"length"<<length<<endl;
    cout<<endl;
}
bool  Cqueue::isEmpty(){
    return front == rear?true:false;
}

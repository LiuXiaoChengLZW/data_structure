//
//  Graph.hpp
//  图的广度优先
//
//  Created by 刘晨 on 2018/11/26.
//  Copyright © 2018 刘晨. All rights reserved.
//

#ifndef Graph_hpp
#define Graph_hpp

#include <stdio.h>
#include <iostream>
using namespace std;
#define VERTEXNUM 100
#define dataType char
typedef struct node{
    dataType data;
    bool isAccess;
}node;
class Graph{
private:
    node vertex[VERTEXNUM];//顶点表
    int edge[VERTEXNUM][VERTEXNUM];//边表
    int vertexNum;//顶点个数
    int edgeNum;//边的个数
    
    int locate(dataType data);//在顶点表中找data的位置
    bool isHaveNevEdge(dataType data);//data是不是还有邻接点
    void move(dataType data);//若一个顶点被删除后，没有邻接点，那就从顶点表中删除这个元素，然后在顶点表和边表中都要移动数据元素。
    int  getFirstVertex(dataType data);//得到元素的第一个邻接点位置。
    int  getNextNevVertex(dataType data,dataType  FrontData);//得到data顶点从Frontdata之后的第一个顶点位置。
    
public:
    void init(int vertex ,int edgeNum);//初始化边数和顶点数。并且初始化边表的数组。
     void  create();//创建图
    void printGraph();//输出
    void addEdge(dataType start,dataType end);//添加一条边
    void deleteEdege(dataType start,dataType end);//删除一条边
    void breadthFirstSearch(dataType data);//广度优先遍历
};
#endif /* Graph_hpp */

//
//  Tree.hpp
//  双亲孩子表示法
//
//  Created by 刘晨 on 2018/11/28.
//  Copyright © 2018 刘晨. All rights reserved.
//

#ifndef Tree_hpp
#define Tree_hpp

#include <stdio.h>
#include <iostream>
using namespace std;
#define  MAXSIZE 100
#define dataType char
typedef struct node{
    int position;
    node * next;
}node;
typedef struct Box{
    dataType data;
    int ParentPosition;
    node *head;
}Box;
class Tree{
private:
    Box base[MAXSIZE];
    int vertexNum;
    
    int locate(dataType data);
    bool isLeadf(dataType data);
    void move(dataType data);
public:
    void init(int vertexNum);
    void create();
    void printTree();
    void printParent(dataType data);
    void printChild(dataType data);
    void addChild(dataType parent,dataType child);
    void deleteNode(dataType data);
};


#endif /* Tree_hpp */

//
//  Tree.cpp
//  双亲孩子表示法
//
//  Created by 刘晨 on 2018/11/28.
//  Copyright © 2018 刘晨. All rights reserved.
//

#include "Tree.hpp"
void Tree::init(int vertexNum){
    this->vertexNum = vertexNum;
}
void Tree::create(){
    cout<<"please input Tree all Node\n";
    for (int i = 0; i<vertexNum; i++) {
        cin>>base[i].data;
        base[i].ParentPosition = -1;
        base[i].head = NULL;
    }
    for (int i = 0; i<vertexNum; i++) {
        cout<<"please input Parent and Child\n";
        dataType data;
        cin>>data;
        int  position = locate(data);
        node *child;
        int childPosition = -1;
        while (data != '.') {
            childPosition = locate(data);
            base[childPosition].ParentPosition = position;
            child = new node;
            child->position = childPosition;
            child->next = base[position].head;
            base[childPosition].head = child;
        }
 }
}
void Tree::printChild(dataType data){
    int position = locate(data);
    node *p = base[position].head;
    while (p != NULL) {
        cout<<base[p->position].data<<",";
        p = p->next;
    }
    cout<<endl;
}
void Tree::printParent(dataType data){
    int position = locate(data);
    cout<<base[base[position].ParentPosition].data<<",";
    cout<<endl;
}
void Tree::printTree(){
    for (int i =0; i<vertexNum; i++) {
        printParent(base[i].data);
        printChild(base[i].data);
    }
}

void Tree::addChild(dataType parent, dataType child){
    int parPosition = locate(parent);
    int childPosition = locate(child);
    if(parPosition != -1 && childPosition == -1){
        base[vertexNum].data = child;
        base[vertexNum].head = NULL;
        base[vertexNum].ParentPosition = parPosition;
        init(vertexNum+1);
        node *p = new node;
        p ->position = vertexNum-1;
        p->next = base[parPosition].head;
        base[parPosition].head = p;
    }else{
        cout<<"error\n";
        return;
    }
}
void Tree::deleteNode(dataType data){
    /*
     删除分为两种情况
     1:删除的结点为叶子结点
     2:删除的结点不是叶子结点而是中间结点怎么办。类似于删除中间结点后
     这个结点和这个结点父亲结点怎么联系，这这里规定为这个结点的孩子几点的第一个结点代替这个结点的位置；
     */
    int positon = locate(data);
    if(isLeadf(data) == true ){
        node *parent = base[base[positon].ParentPosition].head;
        while (parent != NULL) {
            if(parent->position == positon){
                
            }
        }
    }
}
